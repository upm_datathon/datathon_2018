from . import db
from .bases import Model, BaseConstant

class Data(Model):
    __tablename__ = 'data'
    # COLUMNS
    ID = db.Column(db.Integer,primary_key=True)
    CP_CLIENTE = db.Column(db.Integer)
    CP_COMERCIO = db.Column(db.Integer)
    SECTOR = db.Column(db.String(100))
    DIA = db.Column(db.Date)
    FRANJA_HORARIA = db.Column(db.String(100))
    IMPORTE = db.Column(db.Float)
    NUM_OP = db.Column(db.Integer)
    PROP_SEX = db.Column(db.Float)
    PROP_R_BAJA = db.Column(db.Float)
    PROP_R_MEDIA = db.Column(db.Float)
    PROP_R_ALTA = db.Column(db.Float)
    PROP_JOVEN = db.Column(db.Float)
    PROP_ADULTO = db.Column(db.Float)
    PROP_PENSIONISTA = db.Column(db.Float)

DATA = BaseConstant('Data')

class Distancia(Model):
    __tablename__ = 'distancia'
    # COLUMNS
    id = db.Column(db.Integer,primary_key=True)
    municipio = db.Column(db.String(100))
    distancia = db.Column(db.Float)
    tiempo = db.Column(db.Integer)


DISTANCIA = BaseConstant('Distancia')

class Municipio(Model):
    __tablename__ = 'municipio'
    # COLUMNS
    id = db.Column(db.Integer,primary_key=True)
    codigo_postal = db.Column(db.Integer)
    municipio = db.Column(db.String(100))

MUNICIPIO = BaseConstant('Municipio')

class Estadisticas(Model):
    __tablename__ = 'estadisticas'
    # COLUMNS
    id = db.Column(db.Integer,primary_key=True)
    municipio = db.Column(db.String(100))
    tipo = db.Column(db.String(45))
    sexo = db.Column(db.String(45))
    fecha = db.Column(db.Date)
    cantidad = db.Column(db.Integer)

ESTADISTICAS = BaseConstant('Estadisticas')
