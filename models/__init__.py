from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .database import Data, DATA, Municipio, MUNICIPIO, Estadisticas, ESTADISTICAS, Distancia, DISTANCIA

