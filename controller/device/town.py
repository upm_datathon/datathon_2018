from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data,Municipio,Estadisticas,Distancia)
from sqlalchemy import and_,or_,func
from datetime import datetime
from helpers.helpers import murcia_coordinates
import unidecode

municipios_module = Blueprint("municipios",__name__,template_folder="templates")

# DESCRIPTION: Pagina Principal
@municipios_module.route('/municipios', methods=['GET'])
def base():
    JSON = {}
    JSON["treemap"] = treemap()
    return render_template('device/main/municipios.html',DATA = JSON)

#MÉTODO QUE EXTRAE LOS CÓDIGOS POSTALES DE UN MUNICIPIO
def get_zip(municipio):
    if municipio != "Todos los municipios":
        data = Municipio.query.filter(Municipio.municipio==municipio).all()
    else:
        data = Municipio.query.all()
    ZIP = []
    for d in data:
        ZIP.append(d.codigo_postal)
    return ZIP 


def treemap():
    MUNICIPIO = {}
    DATA = {}
    JSON = []
    DATA["importe"] = {}
    DATA["operaciones"] = {}
    data = Municipio.query.all()
    for d in data:
        if d.codigo_postal not in MUNICIPIO:
            MUNICIPIO[d.codigo_postal] = []
        MUNICIPIO[d.codigo_postal].append(d.municipio)
    data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).group_by(Data.CP_CLIENTE).all()
    for d in data:
        for m in MUNICIPIO[d[0]]:
            if m not in DATA["importe"]:
                DATA["importe"][m] = 0
                DATA["operaciones"][m] = 0
            DATA["importe"][m] += int(d[2])
            DATA["operaciones"][m] += int(d[1])
    for municipio in DATA["importe"]:
        data = data_town2(municipio)
        aux = {"renta":data["ingreso"],"comercio":round(data["comercio"]/data["poblacion"],2),"distancia":data["distancia"],"tiempo":data["tiempo"],"poblacion":data["poblacion"],"importe":int(DATA["importe"][municipio]),"operaciones":int(DATA["operaciones"][municipio]),"importe/operaciones":round(DATA["importe"][municipio]/DATA["operaciones"][municipio],1),"municipio":municipio,"paro":round(int(data["paro"])/int(data["poblacion"]),2)}
        JSON.append(aux)
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL MAPA CON FILTROS
@municipios_module.route('/map_town', methods=['GET'])
def map_town():
    municipio = get_zip(request.args.get('municipio'))
    JSON = {"data":[],"max":0}
    for i in murcia_coordinates:
        if int(i["properties"]["codigopostal"]) in municipio:
            JSON["data"].append(i)
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS BÁSICOS DEL MUNICIPIO SELECCIONADO
@municipios_module.route('/data_town', methods=['GET'])
def data_town():
    municipio = request.args.get('municipio')
    ZIP = get_zip(municipio)
    JSON = {"ingreso":0,"poblacion":0,"hombres":0,"mujeres":0,"distancia":0,"tiempo":0,"vehiculos":0,"comercio":0,"farmacias":0,"paro":0}
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="renta")).all()
    for d in data:
        JSON["ingreso"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="comercio")).all()
    for d in data:
        JSON["comercio"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="farmacias")).all()
    for d in data:
        JSON["farmacias"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="vehiculos")).all()
    for d in data:
        JSON["vehiculos"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="ambos",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["poblacion"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="mujeres",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["mujeres"] = d.cantidad   
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="hombres",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["hombres"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="paro",Estadisticas.sexo =="ambos",Estadisticas.fecha=="2017-09-01")).all()
    for d in data:
        JSON["paro"] = d.cantidad
    data = Distancia.query.filter(and_(Distancia.municipio==municipio)).all()
    for d in data:
        JSON["distancia"] = d.distancia
        JSON["tiempo"] = d.tiempo
        
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS BÁSICOS DEL MUNICIPIO SELECCIONADO
def data_town2(municipio):
    ZIP = get_zip(municipio)
    JSON = {"ingreso":0,"poblacion":0,"hombres":0,"mujeres":0,"distancia":0,"tiempo":0,"vehiculos":0,"comercio":0,"farmacias":0,"paro":0}
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="renta")).all()
    for d in data:
        JSON["ingreso"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="comercio")).all()
    for d in data:
        JSON["comercio"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="farmacias")).all()
    for d in data:
        JSON["farmacias"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="vehiculos")).all()
    for d in data:
        JSON["vehiculos"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="ambos",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["poblacion"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="mujeres",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["mujeres"] = d.cantidad   
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="poblacion",Estadisticas.sexo =="hombres",Estadisticas.fecha=="2016-12-01")).all()
    for d in data:
        JSON["hombres"] = d.cantidad
    data = Estadisticas.query.filter(and_(Estadisticas.municipio==municipio,Estadisticas.tipo=="paro",Estadisticas.sexo =="ambos",Estadisticas.fecha=="2017-09-01")).all()
    for d in data:
        JSON["paro"] = d.cantidad
    data = Distancia.query.filter(and_(Distancia.municipio==municipio)).all()
    for d in data:
        JSON["distancia"] = d.distancia
        JSON["tiempo"] = d.tiempo
        
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DE BURBUJAS
@municipios_module.route('/bubbles', methods=['GET'])
def bubbles():
    JSON = []
    municipio = request.args.get('municipio')
    ZIP = get_zip(municipio)
    data = db.session.query(Data.SECTOR,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.CP_CLIENTE.in_(ZIP))).group_by(Data.SECTOR).all()
    for d in data:
        aux = {"municipio":municipio,"sector":d[0],"importe":int(d[2]),"importe promedio":round(int(d[2])/int(d[1]),2)}
        JSON.append(aux)
    return jsonify(JSON)

