from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data,Municipio)
from sqlalchemy import and_,or_,func
from datetime import datetime
import unidecode
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift

fourier_module = Blueprint("fourier",__name__,template_folder="templates")

# DESCRIPTION: Pagina Principal
@fourier_module.route('/fourier', methods=['GET'])
def base():
    return render_template('device/main/fourier.html')

#MÉTODO QUE EXTRAE LOS CÓDIGOS POSTALES DE UN MUNICIPIO
def get_zip(municipio):
    if municipio != "Todos los municipios":
        data = Municipio.query.filter(Municipio.municipio==municipio).all()
    else:
        data = Municipio.query.all()
    ZIP = []
    for d in data:
        ZIP.append(d.codigo_postal)
    return ZIP 

#MÉTODO QUE CALCULA LA TRANSFORMADA DE FOURIER 
@fourier_module.route('/fft_data', methods=['GET'])
def fft_data():
    JSON = {"data":[],"fft":[]}
    sector = request.args.get('sector')
    municipio = request.args.get('municipio')
    ZIP = get_zip(municipio)
    vector = []
    if sector != "TODOS LOS SECTORES":
        data = db.session.query(func.sum(Data.NUM_OP)).filter(and_(Data.CP_CLIENTE.in_(ZIP),Data.SECTOR == sector)).group_by(Data.DIA).order_by(Data.DIA).all()
    else:
        data = db.session.query(func.sum(Data.NUM_OP)).filter(and_(Data.CP_CLIENTE.in_(ZIP))).group_by(Data.DIA).order_by(Data.DIA).all()
    for d in data:
        vector.append(d[0])
    max_vector = max(vector)
    for i in range(len(vector)):
        vector[i] /= max_vector 
    vector = np.array(vector)
    N = len(vector)
    x = np.arange(N)
    y = vector
    yf = 2.0/N*np.abs(fft(y)[0:N//2])
    xf  =  np . linspace ( 0.0 ,  N//2,  N // 2 )
    x = x.tolist()
    y = y.tolist()
    xf = xf.tolist()
    xy = yf.tolist()
    for i in range(len(x)):
        aux = {"municipio":municipio,"tiempo":float(x[i]),"amplitud":float(y[i])}
        JSON["data"].append(aux)
    
    for i in range(len(xf)):
        aux = {"municipio":municipio,"|y(f)|":float(yf[i]),"frecuencia (dias)":float(xf[i])}
        JSON["fft"].append(aux)
    return jsonify(JSON)
