from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data,Municipio)
from helpers.helpers import murcia_coordinates
from sqlalchemy import and_,or_,func
from datetime import datetime
import unidecode

graph_module = Blueprint("graph",__name__,template_folder="templates")

# PARÁMETROS GLOBALES DE LOS MÉTODOS
# a: FECHA INICIAL
# b: FECHA FINAL
# EN EL CASO DE LOS SERVICIOS EJECUTADOS POR GET, LOS PARÁMETROS SON LLAMADOS DIRECTAMENTE POR AJAX

# DESCRIPTION: Pagina Principal
@graph_module.route('/visualizaciones', methods=['GET','POST'])
def base():
    if request.method == 'GET':
        a = '2015-10-01'
        b = '2017-09-30'
        c = 'Todos los municipios'
        DATA = {}
        DATA["barchart"] = barchart(a,b,c)
        DATA["linechart"] = linechart_base(a,b,c)
        DATA["piechart_gender"] = piechart_gender_base(a,b,c)
        DATA["piechart_age"] = piechart_age_base(a,b,c)
        DATA["piechart_status"] = piechart_status_base(a,b,c)
        DATA["heatmap"] = heatmap_base(a,b,c)
        DATA["map"] = map_base(a,b,c)
        DATA["radarchart"] = radarchart_base(a,b,c)
        DATA["treemap"] = treemap_base(a,b,c)
        DATA["options"] = {"datea":a,"dateb":b,"municipio":c}
        return render_template('device/main/visualizaciones.html',DATA=DATA)
    elif request.method == 'POST':
        a = request.form['datea']
        b = request.form['dateb']
        c = request.form['municipio']
        if a == "":
            a = '2015-10-01'
        if b == "":
            b = '2017-09-30'
        DATA = {}
        DATA["barchart"] = barchart(a,b,c)
        DATA["linechart"] = linechart_base(a,b,c)
        DATA["piechart_gender"] = piechart_gender_base(a,b,c)
        DATA["piechart_age"] = piechart_age_base(a,b,c)
        DATA["piechart_status"] = piechart_status_base(a,b,c)
        DATA["heatmap"] = heatmap_base(a,b,c)
        DATA["map"] = map_base(a,b,c)
        DATA["radarchart"] = radarchart_base(a,b,c)
        DATA["treemap"] = treemap_base(a,b,c)
        DATA["options"] = {"datea":a,"dateb":b,"municipio":c}
        return render_template('device/main/visualizaciones.html',DATA=DATA)

#MÉTODO QUE EXTRAE LOS CÓDIGOS POSTALES DE UN MUNICIPIO
def get_zip(municipio):
    if municipio != "Todos los municipios":
        data = Municipio.query.filter(Municipio.municipio==municipio).all()
    else:
        data = Municipio.query.all()
    ZIP = []
    for d in data:
        ZIP.append(d.codigo_postal)
    return ZIP    
    
#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL BARCHART
def barchart(a,b,c):
    JSON = []
    municipio = get_zip(c)
    data = db.session.query(Data.SECTOR,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.SECTOR).all()
    for d in data:
        aux = {"sector":d[0],"operaciones":int(d[1]),"importe":d[2]}
        JSON.append(aux)
    
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL LINECHART CON FILTROS
@graph_module.route('/linechart', methods=['GET'])
def linechart():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    data = db.session.query(Data.DIA,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.SECTOR == sector,Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.DIA).order_by(Data.DIA).all()
    for d in data:
        aux = {"sector":sector,"fecha":d[0].strftime("%Y/%m/%d"),"operaciones":int(d[1]),"importe":d[2]}
        JSON.append(aux)
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL LINECHART INICIAL
def linechart_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    data = db.session.query(Data.DIA,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.DIA).order_by(Data.DIA).all()
    for d in data:
        aux = {"sector":"Todos los sectores","fecha":d[0].strftime("%Y/%m/%d"),"operaciones":int(d[1]),"importe":int(d[2])}
        JSON.append(aux)
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART POR GÉNERO INICIAL
def piechart_gender_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    AUX = {"hombres":{},"mujeres":{}}
    data = db.session.query(func.sum(Data.NUM_OP-Data.NUM_OP*Data.PROP_SEX),func.sum(Data.NUM_OP*Data.PROP_SEX)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["mujeres"]["operaciones"] = d[0]
        AUX["hombres"]["operaciones"] = d[1]
    data = db.session.query(func.sum(Data.IMPORTE-Data.IMPORTE*Data.PROP_SEX),func.sum(Data.IMPORTE*Data.PROP_SEX)).filter(Data.NUM_OP == 1,Data.CP_CLIENTE.in_(municipio)).all()
    for d in data:
        AUX["mujeres"]["importe"] = d[0]/float(d[0]+d[1])
        AUX["hombres"]["importe"] = d[1]/float(d[0]+d[1])
    JSON.append({"tipo":"hombres","operaciones":AUX["hombres"]["operaciones"],"importe":AUX["hombres"]["importe"]})
    JSON.append({"tipo":"mujeres","operaciones":AUX["mujeres"]["operaciones"],"importe":AUX["mujeres"]["importe"]})
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART POR GÉNERO CON FILTROS
@graph_module.route('/piechart_gender', methods=['GET'])
def piechart_gender():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    AUX = {"hombres":{},"mujeres":{}}
    data = db.session.query(func.sum(Data.NUM_OP-Data.NUM_OP*Data.PROP_SEX),func.sum(Data.NUM_OP*Data.PROP_SEX)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["mujeres"]["operaciones"] = d[0]
        AUX["hombres"]["operaciones"] = d[1]
    data = db.session.query(func.sum(Data.IMPORTE-Data.IMPORTE*Data.PROP_SEX),func.sum(Data.IMPORTE*Data.PROP_SEX)).filter(and_(Data.NUM_OP == 1,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["mujeres"]["importe"] = d[0]/float(d[0]+d[1])
        AUX["hombres"]["importe"] = d[1]/float(d[0]+d[1])
    JSON.append({"tipo":"hombres","operaciones":AUX["hombres"]["operaciones"],"importe":AUX["hombres"]["importe"]})
    JSON.append({"tipo":"mujeres","operaciones":AUX["mujeres"]["operaciones"],"importe":AUX["mujeres"]["importe"]})
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART ETARIO INICIAL
def piechart_age_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    AUX = {"jóvenes":{},"adultos":{},"pensionistas":{}}
    data = db.session.query(func.sum(Data.NUM_OP*Data.PROP_JOVEN),func.sum(Data.NUM_OP*Data.PROP_ADULTO),func.sum(Data.NUM_OP*Data.PROP_PENSIONISTA)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["jóvenes"]["operaciones"] = d[0]
        AUX["adultos"]["operaciones"] = d[1]
        AUX["pensionistas"]["operaciones"] = d[2]
        
    
    data = db.session.query(func.sum(Data.IMPORTE*Data.PROP_JOVEN),func.sum(Data.IMPORTE*Data.PROP_ADULTO),func.sum(Data.IMPORTE*Data.PROP_PENSIONISTA),func.sum(Data.NUM_OP*Data.PROP_JOVEN),func.sum(Data.NUM_OP*Data.PROP_ADULTO),func.sum(Data.NUM_OP*Data.PROP_PENSIONISTA)).filter(and_(Data.NUM_OP == 1,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        importea = d[0]/float(d[3])
        importeb = d[1]/float(d[4])
        importec = d[2]/float(d[5])
        AUX["jóvenes"]["importe"] = importea/float(importea+importeb+importec)
        AUX["adultos"]["importe"] = importeb/float(importea+importeb+importec)
        AUX["pensionistas"]["importe"] = importec/float(importea+importeb+importec)
    
    
    JSON.append({"tipo":"jóvenes","operaciones":AUX["jóvenes"]["operaciones"],"importe":AUX["jóvenes"]["importe"]})
    JSON.append({"tipo":"adultos","operaciones":AUX["adultos"]["operaciones"],"importe":AUX["adultos"]["importe"]})
    JSON.append({"tipo":"pensionistas","operaciones":AUX["pensionistas"]["operaciones"],"importe":AUX["pensionistas"]["importe"]})
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART ETARIO CON FILTROS
@graph_module.route('/piechart_age', methods=['GET'])
def piechart_age():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    AUX = {"jóvenes":{},"adultos":{},"pensionistas":{}}
    data = db.session.query(func.sum(Data.NUM_OP*Data.PROP_JOVEN),func.sum(Data.NUM_OP*Data.PROP_ADULTO),func.sum(Data.NUM_OP*Data.PROP_PENSIONISTA)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["jóvenes"]["operaciones"] = d[0]
        AUX["adultos"]["operaciones"] = d[1]
        AUX["pensionistas"]["operaciones"] = d[2]
    data = db.session.query(func.sum(Data.IMPORTE*Data.PROP_JOVEN),func.sum(Data.IMPORTE*Data.PROP_ADULTO),func.sum(Data.IMPORTE*Data.PROP_PENSIONISTA),func.sum(Data.NUM_OP*Data.PROP_JOVEN),func.sum(Data.NUM_OP*Data.PROP_ADULTO),func.sum(Data.NUM_OP*Data.PROP_PENSIONISTA)).filter(and_(Data.SECTOR == sector,Data.NUM_OP == 1,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        importea = d[0]/float(d[3])
        importeb = d[1]/float(d[4])
        importec = d[2]/float(d[5])
        AUX["jóvenes"]["importe"] = importea/float(importea+importeb+importec)
        AUX["adultos"]["importe"] = importeb/float(importea+importeb+importec)
        AUX["pensionistas"]["importe"] = importec/float(importea+importeb+importec)
    JSON.append({"tipo":"jóvenes","operaciones":AUX["jóvenes"]["operaciones"],"importe":AUX["jóvenes"]["importe"]})
    JSON.append({"tipo":"adultos","operaciones":AUX["adultos"]["operaciones"],"importe":AUX["adultos"]["importe"]})
    JSON.append({"tipo":"pensionistas","operaciones":AUX["pensionistas"]["operaciones"],"importe":AUX["pensionistas"]["importe"]})
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART ECONÓMICO INICIAL
def piechart_status_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    AUX = {"ingresos bajos":{},"ingresos medios":{},"ingresos altos":{}}
    data = db.session.query(func.sum(Data.NUM_OP*Data.PROP_R_BAJA),func.sum(Data.NUM_OP*Data.PROP_R_MEDIA),func.sum(Data.NUM_OP*Data.PROP_R_ALTA)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["ingresos bajos"]["operaciones"] = d[0]
        AUX["ingresos medios"]["operaciones"] = d[1]
        AUX["ingresos altos"]["operaciones"] = d[2]
    
    data = db.session.query(func.sum(Data.IMPORTE*Data.PROP_R_BAJA),func.sum(Data.IMPORTE*Data.PROP_R_MEDIA),func.sum(Data.IMPORTE*Data.PROP_R_ALTA),func.sum(Data.NUM_OP*Data.PROP_R_BAJA),func.sum(Data.NUM_OP*Data.PROP_R_MEDIA),func.sum(Data.NUM_OP*Data.PROP_R_ALTA)).filter(and_(Data.NUM_OP == 1,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        importea = d[0]/float(d[3])
        importeb = d[1]/float(d[4])
        importec = d[2]/float(d[5])
        AUX["ingresos bajos"]["importe"] = importea/float(importea+importeb+importec)
        AUX["ingresos medios"]["importe"] = importeb/float(importea+importeb+importec)
        AUX["ingresos altos"]["importe"] = importec/float(importea+importeb+importec)

    JSON.append({"tipo":"ingresos bajos","operaciones":AUX["ingresos bajos"]["operaciones"],"importe":AUX["ingresos bajos"]["importe"]})
    JSON.append({"tipo":"ingresos medios","operaciones":AUX["ingresos medios"]["operaciones"],"importe":AUX["ingresos medios"]["importe"]})
    JSON.append({"tipo":"ingresos altos","operaciones":AUX["ingresos altos"]["operaciones"],"importe":AUX["ingresos altos"]["importe"]})
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL PIECHART ECONÓMICO CON FILTROS
@graph_module.route('/piechart_status', methods=['GET'])
def piechart_status():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    AUX = {"ingresos bajos":{},"ingresos medios":{},"ingresos altos":{}}
    data = db.session.query(func.sum(Data.NUM_OP*Data.PROP_R_BAJA),func.sum(Data.NUM_OP*Data.PROP_R_MEDIA),func.sum(Data.NUM_OP*Data.PROP_R_ALTA)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        AUX["ingresos bajos"]["operaciones"] = d[0]
        AUX["ingresos medios"]["operaciones"] = d[1]
        AUX["ingresos altos"]["operaciones"] = d[2]
    data = db.session.query(func.sum(Data.IMPORTE*Data.PROP_R_BAJA),func.sum(Data.IMPORTE*Data.PROP_R_MEDIA),func.sum(Data.IMPORTE*Data.PROP_R_ALTA),func.sum(Data.NUM_OP*Data.PROP_R_BAJA),func.sum(Data.NUM_OP*Data.PROP_R_MEDIA),func.sum(Data.NUM_OP*Data.PROP_R_ALTA)).filter(and_(Data.NUM_OP == 1,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).all()
    for d in data:
        importea = d[0]/float(d[3])
        importeb = d[1]/float(d[4])
        importec = d[2]/float(d[5])
        AUX["ingresos bajos"]["importe"] = importea/float(importea+importeb+importec)
        AUX["ingresos medios"]["importe"] = importeb/float(importea+importeb+importec)
        AUX["ingresos altos"]["importe"] = importec/float(importea+importeb+importec)
    JSON.append({"tipo":"ingresos bajos","operaciones":AUX["ingresos bajos"]["operaciones"],"importe":AUX["ingresos bajos"]["importe"]})
    JSON.append({"tipo":"ingresos medios","operaciones":AUX["ingresos medios"]["operaciones"],"importe":AUX["ingresos medios"]["importe"]})
    JSON.append({"tipo":"ingresos altos","operaciones":AUX["ingresos altos"]["operaciones"],"importe":AUX["ingresos altos"]["importe"]})
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL HEATMAP INICIAL
def heatmap_base(a,b,c):
    JSON = {1:[],2:[],3:[],4:[],5:[]}
    cp_comercio = [30001,30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30100,30151,30152,30158]
    data = db.session.query(Data.CP_CLIENTE,Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(cp_comercio))).group_by(Data.CP_CLIENTE,Data.CP_COMERCIO).order_by(Data.CP_CLIENTE,Data.CP_COMERCIO).all()
    j = 1
    CP_CLIENTE = []
    CP_AUX = []
    val_aux = 0
    for d in data:
        if d[0] not in CP_CLIENTE:
            CP_CLIENTE.append(d[0])
        if len(CP_CLIENTE) % 37 == 0 and val_aux != len(CP_CLIENTE):
            j += 1
            val_aux = len(CP_CLIENTE)
        CP_AUX.append(str(d[0])+"-"+str(d[1]))
        aux = {"sector":"Todos los sectores","CP_CLIENTE":int(d[0]),"CP_COMERCIO":int(d[1]),"operaciones":int(d[2]),"importe":int(d[3])}
        JSON[j].append(aux)
    for cp in cp_comercio:
        for cp2 in cp_comercio:
            text1 = str(cp)+"-"+str(cp2)
            text2 = str(cp2)+"-"+str(cp)
            if text1 not in CP_AUX or text2 not in CP_AUX:
                aux = {"sector":"Todos los sectores","CP_CLIENTE":int(cp),"CP_COMERCIO":int(cp2),"operaciones":int(0),"importe":int(0)}
                JSON[1].append(aux)
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL HEATMAP CON FILTROS
@graph_module.route('/heatmap', methods=['GET'])
def heatmap():
    JSON = {1:[],2:[],3:[],4:[],5:[]}
    cp_comercio = [30001,30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30100,30151,30152,30158]
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    data = db.session.query(Data.CP_CLIENTE,Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(cp_comercio))).group_by(Data.CP_CLIENTE,Data.CP_COMERCIO).order_by(Data.CP_CLIENTE,Data.CP_COMERCIO).all()
    j = 1
    CP_CLIENTE = []
    CP_AUX = []
    val_aux = 0
    for d in data:
        if d[0] not in CP_CLIENTE:
            CP_CLIENTE.append(d[0])
        if len(CP_CLIENTE) % 37 == 0 and val_aux != len(CP_CLIENTE):
            j += 1
            val_aux = len(CP_CLIENTE)
        CP_AUX.append(str(d[0])+"-"+str(d[1]))
        aux = {"sector":"Todos los sectores","CP_CLIENTE":int(d[0]),"CP_COMERCIO":int(d[1]),"operaciones":int(d[2]),"importe":int(d[3])}
        JSON[j].append(aux)
    for cp in cp_comercio:
        for cp2 in cp_comercio:
            text1 = str(cp)+"-"+str(cp2)
            text2 = str(cp2)+"-"+str(cp)
            if text1 not in CP_AUX or text2 not in CP_AUX:
                aux = {"sector":"Todos los sectores","CP_CLIENTE":int(cp),"CP_COMERCIO":int(cp2),"operaciones":int(1),"importe":int(1)}
                JSON[1].append(aux)
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL MAPA INICIAL
def map_base(a,b,c):
    JSON = {}
    CLIENTE = {}
    COMERCIO = {}
    municipio = get_zip(c)
    MAX = {"cliente_op":[],"cliente_im":[],"comercio_op":[],"comercio_im":[]}
    data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE).order_by(Data.CP_CLIENTE).all()
    for d in data:
        if d[0] not in CLIENTE:
            CLIENTE[d[0]] = {"operaciones":"","importe":""}
        CLIENTE[d[0]]["operaciones"] = int(d[1])
        CLIENTE[d[0]]["importe"] = int(d[2])
    data = db.session.query(Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b)).group_by(Data.CP_COMERCIO).order_by(Data.CP_COMERCIO).all()
    for d in data:
        if d[0] not in COMERCIO:
            COMERCIO[d[0]] = {"operaciones":"","importe":""}
        COMERCIO[d[0]]["operaciones"] = int(d[1])
        COMERCIO[d[0]]["importe"] = int(d[2])
    for i in murcia_coordinates:
        if int(i["properties"]["codigopostal"]) in CLIENTE:
            i["properties"]["cliente_op"] = CLIENTE[int(i["properties"]["codigopostal"])]["operaciones"]
            i["properties"]["cliente_im"] = CLIENTE[int(i["properties"]["codigopostal"])]["importe"]
            MAX["cliente_op"].append(CLIENTE[int(i["properties"]["codigopostal"])]["operaciones"])
            MAX["cliente_im"].append(CLIENTE[int(i["properties"]["codigopostal"])]["importe"])
        else:
            i["properties"]["cliente_op"] = 0
            i["properties"]["cliente_im"] = 0
        if int(i["properties"]["codigopostal"]) in COMERCIO:
            i["properties"]["comercio_op"] = COMERCIO[int(i["properties"]["codigopostal"])]["operaciones"]
            i["properties"]["comercio_im"] = COMERCIO[int(i["properties"]["codigopostal"])]["importe"]
            MAX["comercio_op"].append(COMERCIO[int(i["properties"]["codigopostal"])]["operaciones"])
            MAX["comercio_im"].append(COMERCIO[int(i["properties"]["codigopostal"])]["importe"])
        else:
            i["properties"]["comercio_op"] = 0
            i["properties"]["comercio_im"] = 0
    JSON["data"] = murcia_coordinates
    JSON["max"] = {"cliente_op":max(MAX["cliente_op"]),"cliente_im":max(MAX["cliente_im"]),"comercio_op":max(MAX["comercio_op"]),"comercio_im":max(MAX["comercio_im"])}
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL MAPA CON FILTROS
@graph_module.route('/map', methods=['GET'])
def map():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = {}
    CLIENTE = {}
    COMERCIO = {}
    MAX = {"cliente_op":[],"cliente_im":[],"comercio_op":[],"comercio_im":[]}
    if sector != '':
        data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE).order_by(Data.CP_CLIENTE).all()
    else:
        data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE).order_by(Data.CP_CLIENTE).all()
    for d in data:
        if d[0] not in CLIENTE:
            CLIENTE[d[0]] = {"operaciones":"","importe":""}
        CLIENTE[d[0]]["operaciones"] = int(d[1])
        CLIENTE[d[0]]["importe"] = int(d[2])
    if sector != '':
        data = db.session.query(Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector)).group_by(Data.CP_COMERCIO).order_by(Data.CP_COMERCIO).all()
    else:
        data = db.session.query(Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b)).group_by(Data.CP_COMERCIO).order_by(Data.CP_COMERCIO).all()
    for d in data:
        if d[0] not in COMERCIO:
            COMERCIO[d[0]] = {"operaciones":"","importe":""}
        COMERCIO[d[0]]["operaciones"] = int(d[1])
        COMERCIO[d[0]]["importe"] = int(d[2])
    for i in murcia_coordinates:
        if int(i["properties"]["codigopostal"]) in CLIENTE:
            i["properties"]["cliente_op"] = CLIENTE[int(i["properties"]["codigopostal"])]["operaciones"]
            i["properties"]["cliente_im"] = CLIENTE[int(i["properties"]["codigopostal"])]["importe"]
            MAX["cliente_op"].append(CLIENTE[int(i["properties"]["codigopostal"])]["operaciones"])
            MAX["cliente_im"].append(CLIENTE[int(i["properties"]["codigopostal"])]["importe"])
        else:
            i["properties"]["cliente_op"] = 0
            i["properties"]["cliente_im"] = 0
        if int(i["properties"]["codigopostal"]) in COMERCIO:
            i["properties"]["comercio_op"] = COMERCIO[int(i["properties"]["codigopostal"])]["operaciones"]
            i["properties"]["comercio_im"] = COMERCIO[int(i["properties"]["codigopostal"])]["importe"]
            MAX["comercio_op"].append(COMERCIO[int(i["properties"]["codigopostal"])]["operaciones"])
            MAX["comercio_im"].append(COMERCIO[int(i["properties"]["codigopostal"])]["importe"])
        else:
            i["properties"]["comercio_op"] = 0
            i["properties"]["comercio_im"] = 0
    JSON["data"] = murcia_coordinates
    JSON["max"] = {"cliente_op":max(MAX["cliente_op"]),"cliente_im":max(MAX["cliente_im"]),"comercio_op":max(MAX["comercio_op"]),"comercio_im":max(MAX["comercio_im"])}
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL MAPA AL HACER CLICK EN UN CÓDIGO POSTAL
@graph_module.route('/map_neighbors', methods=['GET'])
def map_neighbors():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    codigo_postal = request.args.get('codigo_postal')
    municipio = get_zip(request.args.get('municipio'))
    tipo = request.args.get('tipo')
    JSON = {}
    DATA = {}
    MAX = {"data_im":[],"data_op":[]}
    if tipo == "cliente":
        if sector != '':
            data = db.session.query(Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE == codigo_postal,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_COMERCIO).order_by(Data.CP_COMERCIO).all()
        else:
            data = db.session.query(Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE == codigo_postal,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_COMERCIO).order_by(Data.CP_COMERCIO).all()
    else:
        if sector != '':
            data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_COMERCIO == codigo_postal,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE).order_by(Data.CP_CLIENTE).all()
        else:
            data = db.session.query(Data.CP_CLIENTE,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_COMERCIO == codigo_postal,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE).order_by(Data.CP_CLIENTE).all()
    for d in data:
        if d[0] not in DATA:
            DATA[d[0]] = {"operaciones":"","importe":""}
        DATA[d[0]]["operaciones"] = int(d[1])
        DATA[d[0]]["importe"] = int(d[2])
    
    for i in murcia_coordinates:
        if int(i["properties"]["codigopostal"]) in DATA:
            i["properties"]["data_op"] = DATA[int(i["properties"]["codigopostal"])]["operaciones"]
            i["properties"]["data_im"] = DATA[int(i["properties"]["codigopostal"])]["importe"]
            MAX["data_op"].append(DATA[int(i["properties"]["codigopostal"])]["operaciones"])
            MAX["data_im"].append(DATA[int(i["properties"]["codigopostal"])]["importe"])
        else:
            i["properties"]["data_op"] = 0
            i["properties"]["data_im"] = 0
        
    JSON["data"] = murcia_coordinates
    try:
        JSON["max"] = {"data_im":max(MAX["data_im"]),"data_op":max(MAX["data_op"])}
    except:
        JSON["max"] = {"data_im":0,"data_op":0}
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL RADARCHART INICIAL
def radarchart_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    data = db.session.query(Data.FRANJA_HORARIA,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.FRANJA_HORARIA).order_by(Data.FRANJA_HORARIA).all()
    for d in data:
        aux = {"name":"Todos los sectores","franja horaria":d[0],"operaciones":int(d[1]),"importe":int(d[2])}
        JSON.append(aux)
    JSON = sorted(JSON,key=lambda x:x["franja horaria"],reverse=False)
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL RADARCHART CON FILTROS
@graph_module.route('/radarchart', methods=['GET'])
def radarchart():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    data = db.session.query(Data.FRANJA_HORARIA,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).group_by(Data.FRANJA_HORARIA).order_by(Data.FRANJA_HORARIA).all()
    for d in data:
        aux = {"name":sector,"franja horaria":d[0],"operaciones":int(d[1]),"importe":int(d[2])}
        JSON.append(aux)
    JSON = sorted(JSON,key=lambda x:x["franja horaria"],reverse=False)
    return jsonify(JSON)

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL TREEMAP INICIAL
def treemap_base(a,b,c):
    JSON = []
    municipio = get_zip(c)
    data = db.session.query(Data.CP_CLIENTE,Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE,Data.CP_COMERCIO).order_by(Data.CP_CLIENTE,Data.CP_COMERCIO).all()
    for d in data:
        aux = {"cliente":d[0],"comercio":"c"+str(d[1]),"operaciones":int(d[2]),"importe":int(d[3])}
        JSON.append(aux)
    return JSON

#MÉTODO QUE EXTRAE LOS DATOS PARA LA VISUALIZACIÓN DEL TREEMAP CON FILTROS
@graph_module.route('/treemap', methods=['GET'])
def treemap():
    a = request.args.get('datea')
    b = request.args.get('dateb')
    sector = request.args.get('sector')
    municipio = get_zip(request.args.get('municipio'))
    JSON = []
    data = db.session.query(Data.CP_CLIENTE,Data.CP_COMERCIO,func.sum(Data.NUM_OP),func.sum(Data.IMPORTE)).filter(and_(Data.DIA >=a,Data.DIA <=b,Data.SECTOR == sector,Data.CP_CLIENTE.in_(municipio))).group_by(Data.CP_CLIENTE,Data.CP_COMERCIO).order_by(Data.CP_CLIENTE,Data.CP_COMERCIO).all()
    for d in data:
        aux = {"cliente":d[0],"comercio":"c"+str(d[1]),"operaciones":int(d[2]),"importe":int(d[3])}
        JSON.append(aux)
    return jsonify(JSON)
    