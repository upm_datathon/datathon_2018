from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data)
from sqlalchemy import and_,or_
from datetime import datetime
from dateutil.relativedelta import *
import unidecode

buy_module = Blueprint("buy",__name__,template_folder="templates")

# DESCRIPTION: Pagina Principal
@buy_module.route('/adelantate', methods=['GET'])
def base():
    return render_template('device/main/planificacion.html')


@buy_module.route('/buy', methods=['GET'])
def buy():
    date = request.args.get('date')
    date_aux = date.split("-")[1]+"-"+date.split("-")[2]
    codigo_postal = request.args.get('codigo_postal')
    comercio = request.args.get('comercio').split(",")
    SPECIAL_DAYS = ['01-01','01-05','01-06','08-15','10-12','11-01','12-06','12-08','24-08','25-08']
    new_date = datetime.strptime(date, '%Y-%m-%d')
    JSON = []
    DATA = {}
    if date_aux not in SPECIAL_DAYS:
        week = new_date.isocalendar()[1]
        day = new_date.weekday()
        year2017 = datetime.strptime("2017-"+str(week)+"-"+str(day),"%Y-%W-%w")
        year2016 = datetime.strptime("2016-"+str(week)+"-"+str(day),"%Y-%W-%w")
        year2015 = datetime.strptime("2015-"+str(week)+"-"+str(day),"%Y-%W-%w")
    else:
        year2017 = datetime.strptime("2017-"+date_aux,"%Y-%m-%d")
        year2016 = datetime.strptime("2016-"+date_aux,"%Y-%m-%d")
        year2015 = datetime.strptime("2015-"+date_aux,"%Y-%m-%d")
    data = Data.query.filter(Data.DIA.in_([year2017,year2016,year2015])).all()
    for d in data:
        if d.FRANJA_HORARIA not in DATA:
            DATA[d.FRANJA_HORARIA] = 0
        DATA[d.FRANJA_HORARIA] += d.NUM_OP
    for h in DATA:
        aux = {"name":','.join(comercio),"operaciones":DATA[h],"franja horaria":h}
        JSON.append(aux)
    JSON = sorted(JSON,key=lambda x:x["franja horaria"],reverse=False)
    return jsonify(JSON)