from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data)
from sqlalchemy import and_,or_
from datetime import datetime
import unidecode

help_module = Blueprint("help",__name__,template_folder="templates")

# DESCRIPTION: Pagina Principal
@help_module.route('/ayuda', methods=['GET'])
def base():
    return render_template('device/main/ayuda.html')
