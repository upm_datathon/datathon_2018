from flask import (Blueprint, jsonify, request, 
                    url_for, render_template)
from models import (db, Data)
from sqlalchemy import and_,or_
from datetime import datetime
import unidecode

device_module = Blueprint("device",__name__,template_folder="templates")

# DESCRIPTION: Pagina Principal
@device_module.route('/', methods=['GET'])
def base():
    return render_template('device/main/base.html')
