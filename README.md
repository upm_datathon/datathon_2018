# VERSION
0.1

# DESCRIPCIÓN
PARA LA EJECUCIÓN DE LA APLICACIÓN SE DEBE DISPONER DE PYTHON 3.0 Y SATISFACER LAS LIBRERÍAS FLASK, SQLALCHEMY Y MYSQL PARA PYTHON
DENTRO DEL REPOSITORIO SE ENCUENTRA LA BASE DE DATOS DATABASE.SQL, LA CUAL DEBE SER CARGADA
EL ARCHIVO CONFIG.PY ES EL ARCHIVO DE CONFIGURACIÓN, EN EL SE DEBEN INGRESAR LAS CREDENCIALES DE CONEXIÓN A LA BASE DE DATOS, SÓLO SE DEBE MODIFICAR LA CONSTANTE SQLALCHEMY_DATABASE_URI.
PARA EJECUTAR EL PROYECTO, UNA VEZ SATISFECHAS LOS REQUERIMIENTOS ANTERIORES, SE DEBE EJECUTAR EL COMANDO "python run.py" Y ENTRAR A LA DIRECCIÓN HTTP://LOCALHOST:5000


Algunos detalles
Para acceder al sitio se debe hacer desde el siguiente link: http://datathon.chaotic-machine.science/
El repositorio del proyecto se encuentra en: https://bitbucket.org/upm_datathon/datathon_2018/
Los resultados se encuentran tanto en el pptx como en el sitio web del proyecto
Las ayudas o manual de usuario se encuentran en el mismo sitio web
El link de wetransfer es el siguiente: https://we.tl/zLcxk4icJz


