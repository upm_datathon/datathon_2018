El trabajo realizado es un sitio web construído mediante el microframework Flask a través de Python, MySQL y SQLAlchemy para el backend, en conjunto con HTML y Javascript para el frontend, utilizando las librerías jQuery, d3, d3Plus y leaflet para los gráficos y materializecss para el diseño, el cual se encuentra disponible en el repositorio bitbucket https://bitbucket.org/upm_datathon/datathon_2018

Las instrucciones de ejecución se encuentran dentro del archivo README.md y el proyecto conserva la estructura modelo vista controlador.
En la carpeta controller se encuentra el controlador, que son los códigos necesarios para el vínculo con la base de datos y los métodos en lenguaje Python que transforman estos datos a salidas del tipo JSON.
En la carpeta templates se encuentra la vista, en ella se encuentran el código HTML y Javascript para la visualización de cada uno de los gráficos.

Todos los códigos se encuentran debidamente comentados, además, la aplicación web incluye ayudas interactivas a modo de manual de usuario (se caracterizan por el signo de interrogación).
Las fuentes de datos externas utilizadas se basaron principalmente a la información demográfica de la provincia de murcia como además de los polígonos de cada uno de los códigos postales.

El objetivo principal de la aplicación es proporcionar una vista amigable y didáctica de las transacciones con tarjeta de la provincia de Murcia, a modo de que el usuario pueda obtener la mayor cantidad de información con la menor cantidad de esfuerzo posible; además es posible visualizar las interacciones entre los distintos códigos postales.

Para acceder al sitio web se debe ingresar a http://fractalnet-works.ddns.net:6006/datathon

