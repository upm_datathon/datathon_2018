import os
from flask import Flask
from flask_cors import CORS
from models import db

app = Flask(__name__, static_url_path='/static')
app.config.from_object('config')

db.init_app(app)

# Models
from models.database import Data, Distancia, Estadisticas, Municipio

with app.app_context():
	db.create_all()

from controller.device.main import device_module
from controller.device.buy import buy_module
from controller.device.graph import graph_module
from controller.device.help import help_module
from controller.device.interpretation import interpretation_module
from controller.device.fourier import fourier_module
from controller.device.town import municipios_module

app.register_blueprint(device_module)
app.register_blueprint(buy_module)
app.register_blueprint(graph_module)
app.register_blueprint(help_module)
app.register_blueprint(interpretation_module)
app.register_blueprint(fourier_module)
app.register_blueprint(municipios_module)
